import co.cata.SecUser
import co.cata.SecRole
import co.cata.SecUserSecRole
import co.cata.blog.Post

class BootStrap {

    def springSecurityService
    def fileService
    
    def init = { servletContext ->
        
        if(true) {
            createDevelopmentData()
        }

    }
    def destroy = {
    }
    
    def createDevelopmentData() {
        def adminRole = new SecRole(authority: 'ADMIN_ROLE').save(failOnError: true);
        def blogPostRole = new SecRole(authority: 'BLOG_POST_ROLE').save(failOnError: true);

        def adminUser = SecUser.findByUsername('admin') ?: new SecUser(username: 'admin', password: 'admin', email: 'admin@cata.co', enabled: true).save(failOnError: true)
        if (!adminUser.authorities.contains(adminRole)) {
            SecUserSecRole.create adminUser, adminRole
        }
        
        def post = new Post(profile: adminUser.profile, title:'Test Post', content: '*Textile* Testing\r\n\r\nI\'m -sure- not sure. Could have been the @code monster@!!1').save(failOnError: true) 
        post.addTags(['Test','#soundatigermakes'])
        post.addTag('meh')
        post.addComment(adminUser.profile, 'Comment 1')
        post.addComment(adminUser.profile, 'Keeping it real')

    }
}
