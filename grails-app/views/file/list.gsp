
<%@ page import="co.cata.cms.File" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'file.label', default: 'File')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-file" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="upload" action="upload"><g:message code="default.upload.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-file" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
                                                <th>Icon</th>
                                                
						<g:sortableColumn property="name" title="${message(code: 'file.name.label', default: 'Name')}" />
					
                                                <g:sortableColumn property="extension" title="${message(code: 'file.extension.label', default: 'Extension')}" />
					
						<g:sortableColumn property="fileGroup" title="${message(code: 'file.fileGroup.label', default: 'Group')}" />
					
						<g:sortableColumn property="size" title="${message(code: 'file.size.label', default: 'Size')}" />
					
						<g:sortableColumn property="counter" title="${message(code: 'file.counter.label', default: 'Counter')}" />
					
                                                <g:sortableColumn property="hash" title="${message(code: 'file.hash.label', default: 'Hash')}" />
                                                
                                                <g:sortableColumn property="owner" title="${message(code: 'file.owner.label', default: 'Owner')}" />
                                                
						<g:sortableColumn property="dateCreated" title="${message(code: 'file.dateCreated.label', default: 'Date Created')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${fileInstanceList}" status="i" var="fileInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                                <td><cata:icon id="${fileInstance.id}" /></td>
                                            
						<td><g:link action="show" id="${fileInstance.id}">${fieldValue(bean: fileInstance, field: "name")}</g:link></td>
					
                                                <td>${fieldValue(bean: fileInstance, field: "extension")}</td>
                                                
						<td>${fieldValue(bean: fileInstance, field: "fileGroup")}</td>
					
						<td><cata:formatSize size="${fileInstance.size}" /></td>
					
						<td>${fieldValue(bean: fileInstance, field: "counter")}</td>
					
                                                <td><cata:formatHash hash="${fileInstance.hash}" /></td>
                                                
                                                <td>${fieldValue(bean: fileInstance, field: "profile.name")}</td>
                                                
						<td><cata:formatDate format="dd-MMM-yyyy hh:mm" date="${fileInstance.dateCreated}" /></td>
                                            
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${fileInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
