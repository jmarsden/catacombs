<%@ page import="co.cata.cms.File" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'file.label', default: 'File')}" />
		<title><g:message code="default.upload.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#upload-file" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="upload-file" class="content scaffold-create" role="main">
			<h1><g:message code="default.upload.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${fileInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${fileInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
                        
                        <g:uploadForm action="save">
                          <fieldset class="form">
                            <div class="fieldcontain">
                              <label for="file"><g:message code="file.label" default="File" /></label>
                              <input type="file" name="file.1" />
                            </div>
                            <div class="fieldcontain ${hasErrors(bean: fileInstance, field: 'name', 'error')} ">
                              <label for="name"><g:message code="file.name.label" default="Name" /></label>
                              <g:textField name="name.1" value="${fileInstance?.name}"/>
                            </div>
                            <div class="fieldcontain ${hasErrors(bean: fileInstance, field: 'publicFlag', 'error')} ">
                              <label for="name"><g:message code="file.publicFlag.label" default="Public?" /></label>
                              <g:checkBox name="publicFlag.1" value="${fileInstance?.publicFlag}"/>
                            </div>
                            
                          </fieldset>
                          <fieldset class="buttons">
                            <g:submitButton name="upload" class="save" value="${message(code: 'default.button.upload.label', default: 'Upload')}" />
                          </fieldset>
                        </g:uploadForm>

		</div>
	</body>
</html>