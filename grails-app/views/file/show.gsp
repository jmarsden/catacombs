
<%@ page import="co.cata.cms.File" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'file.label', default: 'File')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-file" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="upload" action="upload"><g:message code="default.upload.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-file" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
                        <span style="margin: 0.6em 1.25em 0 1.25em; padding: 0.3em 1.8em 1.25em;">
                          <cata:icon id="${fileInstance.id}" />
                        </span>
			<ol class="property-list file">
			
				<g:if test="${fileInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="file.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${fileInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileInstance?.contentType}">
				<li class="fieldcontain">
					<span id="contentType-label" class="property-label"><g:message code="file.contentType.label" default="Content Type" /></span>
					
						<span class="property-value" aria-labelledby="contentType-label"><g:fieldValue bean="${fileInstance}" field="contentType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileInstance?.size}">
				<li class="fieldcontain">
					<span id="size-label" class="property-label"><g:message code="file.size.label" default="Size" /></span>
					
						<span class="property-value" aria-labelledby="size-label"><cata:formatSize size="${fileInstance.size}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileInstance?.counter}">
				<li class="fieldcontain">
					<span id="counter-label" class="property-label"><g:message code="file.counter.label" default="Counter" /></span>
					
						<span class="property-value" aria-labelledby="counter-label"><g:fieldValue bean="${fileInstance}" field="counter"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="file.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${fileInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="file.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${fileInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileInstance?.profile}">
				<li class="fieldcontain">
					<span id="profile-label" class="property-label"><g:message code="file.profile.label" default="Profile" /></span>
					
						<span class="property-value" aria-labelledby="profile-label"><g:link controller="profile" action="show" id="${fileInstance?.profile?.id}">${fileInstance?.profile?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
                        
                        
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${fileInstance?.id}" />
					<!--<g:link class="edit" action="edit" id="${fileInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>-->
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                        <g:actionSubmit class="download" action="serve" value="${message(code: 'default.button.download.label', default: 'Download')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                </fieldset>
			</g:form>
		</div>
	</body>
</html>
