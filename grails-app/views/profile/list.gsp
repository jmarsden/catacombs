
<%@ page import="co.cata.Profile" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'profile.label', default: 'Profile')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-profile" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-profile" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="displayName" title="${message(code: 'profile.displayName.label', default: 'Display Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'profile.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="firstName" title="${message(code: 'profile.firstName.label', default: 'First Name')}" />
					
						<g:sortableColumn property="lastName" title="${message(code: 'profile.lastName.label', default: 'Last Name')}" />
					
						<g:sortableColumn property="dateOfBirth" title="${message(code: 'profile.dateOfBirth.label', default: 'Date Of Birth')}" />
					
						<th><g:message code="profile.user.label" default="User" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${profileInstanceList}" status="i" var="profileInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${profileInstance.id}">${fieldValue(bean: profileInstance, field: "displayName")}</g:link></td>
					
						<td>${fieldValue(bean: profileInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: profileInstance, field: "firstName")}</td>
					
						<td>${fieldValue(bean: profileInstance, field: "lastName")}</td>
					
						<td><g:formatDate date="${profileInstance.dateOfBirth}" /></td>
					
						<td>${fieldValue(bean: profileInstance, field: "user")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${profileInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
