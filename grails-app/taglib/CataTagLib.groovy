
class CataTagLib {

    static namespace = "cata"
    
    def fileService
    
    def formatSize = { attrs, body ->
        long size = attrs.size
        String unit = 'B'
        out << size << unit
    }
    
    def formatDate = { attrs, body ->
        String format = attrs.format
        Date date = attrs.date
        if(format == null) {
            format = 'dd-MM-yyyy'
        }
        if(date == null) {
            date = new Date()
        }
        out << new java.text.SimpleDateFormat(format).format(date)
    }
    
    def formatHash = { attrs, body ->
        String hash = attrs.hash
        if(hash == null) {
            out << "nohash"
        } else {
            out << "<span class='hash' title='${hash}'><code>" << hash[0..4] << "&#133;" << hash[hash.size()-5..hash.size()-1] << "</code></span>"
        }
    }
    
    def icon = {attrs, body ->
        if(!attrs.id) {
            out << "Configuration Error. Must have an id attribute to use FilePreview"
            return
        }
        Long id = attrs.id
        def fileInstance = co.cata.cms.File.get(id)
        if (!fileInstance) {
            out << "<span>NO PREVIEW</span>"
        } else {
            String serverURL = grailsApplication.config.grails.serverURL
            if(fileInstance.fileGroup == "image") {
                String imageURL = "${serverURL}/file/preview/${id}"
                println(imageURL)
                out << "<img src=\"${imageURL}\" style=\"height: 50px; width: 50px\" alt=\"File\">"
            } else {
                String imageURL = "${serverURL}/images/icon/misc.png"
                out << "<img src=\"${imageURL}\" style=\"height: 50px; width: 50px\" alt=\"File\">"
            }
            
            
            
        }
        
    }
}
