package co.cata.cms

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException

class FileController {

    def fileService
    
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [fileInstanceList: File.list(params), fileInstanceTotal: File.count()]
    }

    // Upload File Controller
    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def upload() {
        [fileInstance: new File(params)]
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def save() {

        def fileInstances = params.file
        def nameInstances = params.name
        def publicFlagInstances = params.publicFlag
        
        if(fileInstances.length != nameInstances.length) {
            println('Error!')
        }
        
        nameInstances.each() { i, name ->
            def file = fileInstances.getAt(i)
            boolean publicFlag = publicFlagInstances.getAt(i)
            
            println("${name} ${file} ${publicFlag}")
            
            fileService.createFile(file, name, publicFlag)
        }
        
        render(view:"list", model: [fileInstanceList: File.list(params), fileInstanceTotal: File.count()])
    }

    def show(Long id) {
        def fileInstance = File.get(id)
        if (!fileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
            return
        }

        [fileInstance: fileInstance]
    }
    
    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def delete(Long id) {
        def fileInstance = File.get(id)
        if (!fileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
            return
        }

        try {
            fileService.deleteFile(fileInstance)
            fileInstance.delete(flush: true)
            
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "show", id: id)
        }
    }
    
    def preview(Long id) {
        cache shared:true, validFor: 3600
        def fileInstance = File.get(id)
        if (!fileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
            return
        }
       
        if(fileInstance.publicFlag) {     
            def file = fileService.getPreview(fileInstance)   
            response.setContentType(fileInstance.contentType)
            response.setHeader("Content-disposition", "attachment;filename=${fileInstance.name}")
            response.setHeader("Content-type", fileInstance.contentType)
            
            response.outputStream << file.newInputStream() 
        } else {
            redirect(action: "servePrivatePreview", id: id)
        }
    }
    
    def serve(Long id) {
        cache shared:true, validFor: 3600
        def fileInstance = File.get(id)
        if (!fileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
            return
        }
       
        if(fileInstance.publicFlag) {     
            fileInstance.counter++
            fileInstance.save()

            def file = fileService.getFile(fileInstance)   
            response.setContentType(fileInstance.contentType)
            response.setHeader("Content-disposition", "attachment;filename=${fileInstance.name}")
            response.setHeader("Content-type", fileInstance.contentType)
            
            response.outputStream << file.newInputStream() 
        } else {
            redirect(action: "servePrivate", id: id)
        }
    }
    
    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def servePrivatePreview(Long id) {
        cache shared:true, validFor: 3600
        def fileInstance = File.get(id)
        if (!fileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
            return
        }
        def file = fileService.getPreview(fileInstance)   
        response.setContentType(fileInstance.contentType)
        response.setHeader("Content-disposition", "attachment;filename=${fileInstance.name}")  
        response.setHeader("Content-type", fileInstance.contentType)
        response.outputStream << file.newInputStream()  
    }
    
    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def servePrivate(Long id) {
        cache shared:true, validFor: 3600
        def fileInstance = File.get(id)
        if (!fileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'file.label', default: 'File'), id])
            redirect(action: "list")
            return
        }
         
        fileInstance.counter++
        fileInstance.save()

        def file = fileService.getFile(fileInstance)   
        response.setContentType(fileInstance.contentType)
        response.setHeader("Content-disposition", "attachment;filename=${fileInstance.name}")  
        response.setHeader("Content-type", fileInstance.contentType)
        response.outputStream << file.newInputStream()  
    }
}
