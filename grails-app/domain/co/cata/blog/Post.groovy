package co.cata.blog

import org.grails.comments.*
import org.grails.taggable.*

import co.cata.Profile

class Post implements Taggable, Commentable {

    static belongsTo = [ profile: Profile ]
    
    Date dateCreated
    Date lastUpdated
    String title
    String content
    
    static constraints = {
        title blank: true, nullable: true
    }
}
