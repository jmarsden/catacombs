package co.cata

import org.grails.comments.*

import co.cata.blog.Post;

class Profile implements Commentable {

    static transients = ['avatar']
    
    static hasOne = [user: SecUser]
    
    static hasMany = [posts: Post]
    
    String name
    String description
    String avatar
    
    String firstName
    String lastName
    Date dateOfBirth
    
    static constraints = {
        name blank: false, nullable: false
        description blank: true, nullable: true
        avatar blank: true, nullable: true
        
        firstName blank: true, nullable: true
        lastName blank: true, nullable: true
        dateOfBirth blank: true, nullable: true
    }
    
    String toString() {
        "Profile for ${name} (${user})"
    }
}
