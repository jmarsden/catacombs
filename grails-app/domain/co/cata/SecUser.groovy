package co.cata

class SecUser {

    Profile profile;
    
    transient springSecurityService

    String username
    String password
    String email
    boolean enabled
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static constraints = {
        username blank: false, unique: true
        password blank: false
        email blank: false
        profile blank: true, nullable: true
    }

    static mapping = {
        password column: '`password`'
    }

    Set<SecRole> getAuthorities() {
        SecUserSecRole.findAllBySecUser(this).collect { it.secRole } as Set
    }
    
    def beforeValidate() {
        if(profile == null) {
            def newProfile = new Profile(user: this, name: this.username)
            newProfile.save(failOnError: true, flush:true)
            profile = newProfile;
        }
    }
    
    def beforeInsert() {        
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }
        
    String toString() {
        "User ${username}"
    }
}
