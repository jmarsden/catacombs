package co.cata.cms

import org.grails.comments.*
import org.grails.taggable.*

import co.cata.Profile

class File implements Taggable, Commentable {

    
    Profile profile
    Date dateCreated
    Date lastUpdated
    String name
    String originalName
    String fileName
    String extension
    String contentType
    String hash
    long size
    
    boolean publicFlag = true
    int counter = 0
    String fileGroup
    
    static constraints = {
        profile blank: false, nullable: true
        name blank: false, nullable: false
        originalName blank: false, nullable: true
        fileName blank: false, nullable: true
        extension blank: false, nullable: true
        contentType blank: false, nullable: false
        hash blank: true, nullable: true, unique: true
        size blank: false, nullable: false
        fileGroup blank: false, nullable: false
    }
    
}
