package co.cata
    
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.security.MessageDigest
import org.springframework.web.multipart.MultipartFile

class FileService {

    def grailsApplication
    def springSecurityService
    
    def createFile(MultipartFile file, String name = null, publicFlag = true) {
        if(file.isEmpty()) {
            return
        }
        String fileRootPath = grailsApplication.config.catacombs.cms.file.rootFolder
        String originalFilename = ''
        String extension = ''
        String fileName = ''
        def matcher = (file.originalFilename =~ /^([^\\.]*)[\.]?(.*)$/)
        if (matcher.size() > 0 && matcher[0].size() == 3) {
            println(matcher)
            originalFilename = matcher[0][0]
            fileName = matcher[0][1]
            extension = matcher[0][2]
        }
        if(!name) {
            name = fileName
        }
       
        println "Name: ${name} Ex: ${extension}"    
        
        String contentType = file.contentType
        String fileGroup = getGroup(contentType)
        
        long size = file.size
        String hash = generateHash(file)
        Profile profile = springSecurityService.getCurrentUser().profile
        
        println("${originalFilename} ${name} ${contentType} ${size} ${hash} ${profile}")
        
        File destinationFile = new File("${fileRootPath}/${fileGroup}/${hash}")

        println("Destination: ${destinationFile}")
        
        if(!destinationFile.exists()) {
            destinationFile.mkdirs()
        }
        
        new co.cata.cms.File(profile: profile, originalFilename: originalFilename, fileName: fileName, extension: extension, fileGroup: fileGroup, name: name, contentType: contentType, size: size, publicFlag: publicFlag, hash: hash).save()
        file.transferTo(destinationFile) 
    }
    
    def deleteFile(co.cata.cms.File file) {
        String fileRootPath = grailsApplication.config.catacombs.cms.file.rootFolder
        
        File destinationFile = new File("${fileRootPath}/${file.fileGroup}/${file.hash}")
        if(destinationFile.exists()) {
            destinationFile.delete()
        }
    }
    
    def getFile(co.cata.cms.File file) {
        String fileRootPath = grailsApplication.config.catacombs.cms.file.rootFolder
        File destinationFile = new File("${fileRootPath}/${file.fileGroup}/${file.hash}")
        return destinationFile
    }
    
    def getPreview(co.cata.cms.File file) {
        String previewRootFolder = grailsApplication.config.catacombs.cms.preview.rootFolder
        File previewFile = new File("${previewRootFolder}/${file.fileGroup}/${file.hash}")
        if(previewFile.exists()) {
            return previewFile
        } else {
            return createPreview(file)
        }
    }
    
    def createPreview(co.cata.cms.File file) {
        String fileRootPath = grailsApplication.config.catacombs.cms.file.rootFolder
        File destinationFile = new File("${fileRootPath}/${file.fileGroup}/${file.hash}")
        String previewRootFolder = grailsApplication.config.catacombs.cms.preview.rootFolder
        File previewFile = new File("${previewRootFolder}/${file.fileGroup}/${file.hash}")
        
        if(!destinationFile.exists()) {
            // TODO: Throw an exception
            println("No DetinationFile!")
        }
        if(previewFile.exists()) {
            destinationFile.delete() 
        }
        
        BufferedImage originalImage = ImageIO.read(destinationFile);
	int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        
        BufferedImage previewImage = resizeImage(originalImage, type);
        
        if(!previewFile.exists()) {
            previewFile.mkdirs()
        }
        
        println("Values: ${previewImage} ${file.extension} ${previewFile}")
        
	ImageIO.write(previewImage, file.extension, previewFile); 

        return previewFile
    }
    
    def getGroup(final String contentType) {
        if(contentType.contains('/')) {
            String[] explode = contentType.split('/')
            return explode[0]
        }
        else {
            return 'misc'
        }
    }
    
    def BufferedImage resizeImage(BufferedImage originalImage, int type) {
        int maxWidth = 300;
        int maxHeight = 300;
        int imageWidth = 300;
        int imageHeight = 300;
        
        
        imageHeight = originalImage.getHeight() * imageWidth / originalImage.getWidth();
	if (imageHeight > maxHeight)
	{
		// Resize with height instead
		imageWidth = originalImage.getWidth() * maxHeight / originalImage.getHeight();
		imageHeight = maxHeight;
	}

        
        BufferedImage resizedImage = new BufferedImage(imageWidth, imageHeight, type);
	Graphics2D g = resizedImage.createGraphics();
	g.drawImage(originalImage, 0, 0, imageWidth, imageHeight, null);
	g.dispose();	
	g.setComposite(AlphaComposite.Src);
 
	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	g.setRenderingHint(RenderingHints.KEY_RENDERING,
	RenderingHints.VALUE_RENDER_QUALITY);
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	RenderingHints.VALUE_ANTIALIAS_ON);
 
	return resizedImage;
    }
    
    def generateHash(final file) {
        generateMD5(file)
    }
    
    def generateMD5(final file) {
        MessageDigest digest = MessageDigest.getInstance("MD5")
        def is = file.getInputStream()
        byte[] buffer = new byte[8192]
        int read = 0
        while( (read = is.read(buffer)) > 0) {
            digest.update(buffer, 0, read);
        }
        
        byte[] md5sum = digest.digest()
        BigInteger bigInt = new BigInteger(1, md5sum)
        return String.format("%0" + (md5sum.length << 1) + "X", bigInt);
    }
}
